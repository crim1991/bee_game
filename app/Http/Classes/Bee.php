<?php
namespace App\Http\Classes;

use \App\Models\Bee as BeeModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Bee
{
    protected $health;
    protected $damage;
    protected $name;
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function hitBee()
    {
        DB::unprepared(
            "UPDATE bees 
              SET lifespan = lifespan - {$this->damage} 
              WHERE id = '{$this->id}'"
        );
    }

    public function log()
    {
        Session::flash('log', "$this->name with id: $this->id was hitted with $this->damage damage");
    }

    public function respawn()
    {
        BeeModel::where('id', $this->id)
         ->update(['lifespan' => $this->health]);
    }

    public function respawnLog()
    {
        Session::flash('log', "Queen was defeated, game restarted");
    }
}