<?php

namespace App\Http\Classes;


use App\Models\Bee as BeeModel;


class GamePlay
{
    public function run() {
        $rand_bee = BeeModel::getRandomBee();
        $this->fight($rand_bee);
        $this->verifyQueen($rand_bee);
    }

    protected function fight($bee)
    {
        $instance = $this->getBeeInstance($bee);
        $instance->hitBee();
        $instance->log();
    }

    protected function getBeeInstance($bee) {
        $class = "App\Http\Classes\Bees\\" . ucfirst($bee->type);
        return new $class($bee->id);
    }

    protected function verifyQueen($bee) {
        if ($bee->type == 'queen') {
            $get_queen = BeeModel::countQueenWithLifespan($bee->id);
            if (!$get_queen) {
                $this->restartGame();
            }
        }
    }

    protected function restartGame() {
        //get all bees
        $bees = BeeModel::getAllBees();

        foreach ($bees as $bee) {
            $instance = $this->getBeeInstance($bee);
            $instance->respawn();
            $instance->respawnLog();
        }
    }

}