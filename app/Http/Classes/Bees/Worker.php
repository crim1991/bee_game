<?php

namespace App\Http\Classes\Bees;

use App\Http\Classes\Bee;

class Worker extends Bee
{
    protected $health = 75;
    protected $damage = 10;
    protected $name = 'worker';
}