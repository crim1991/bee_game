<?php

namespace App\Http\Classes\Bees;

use App\Http\Classes\Bee;

class Drone extends Bee
{
    protected $health = 50;
    protected $damage = 12;
    protected $name = 'drone';
}