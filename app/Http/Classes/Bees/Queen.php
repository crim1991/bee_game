<?php

namespace App\Http\Classes\Bees;

use App\Http\Classes\Bee;

class Queen extends Bee
{
    protected $health = 100;
    protected $damage = 8;
    protected $name = 'queen';
}