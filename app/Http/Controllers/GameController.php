<?php

namespace App\Http\Controllers;

//use App\Http\Classes\BeeGame\Bee;
use App\Http\Classes\GamePlay;
use App\Models\Bee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GameController extends Controller
{
    public function index()
    {
        //get bees
        $bees = Bee::select('bees.id', 'lifespan', 'type')
            ->join('bee_types', 'bee_types.id', 'bees.type_id')
            ->where('lifespan', '>', 0)
            ->get();

        return view('bee.index', get_defined_vars());
    }

    public function hitBee()
    {
        $game = new GamePlay();
        $game->run();

        return Redirect::back();
    }

}
