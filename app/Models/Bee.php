<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bee extends Model
{
    protected $table = 'bees';
    protected $fillable = ['type_id', 'lifespan'];

    public static function getRandomBee()
    {
        return Bee::select('bees.id', 'type')
            ->join('bee_types', 'bee_types.id', 'bees.type_id')
            ->where('lifespan', '>', 0)
            ->inRandomOrder()->limit(1)->first();
    }

    public static function countQueenWithLifespan($id)
    {
        return Bee::where('id', $id)
        ->where('lifespan', '>', 0)
        ->count();
    }

    public static function getAllBees()
    {
        return Bee::select('bees.id', 'type')
            ->join('bee_types', 'bee_types.id', 'bees.type_id')
            ->get();
    }

}
