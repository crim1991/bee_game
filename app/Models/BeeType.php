<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BeeType extends Model
{
    protected $table = 'bee_types';
    protected $fillable = ['type'];
}
