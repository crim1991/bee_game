<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertBees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bee_types = DB::table('bee_types')->get();

        foreach ($bee_types as $k => $bee) {
            if ($bee->type == 'queen') {
                DB::table('bees')->insert([
                    'type_id' => $bee->id,
                    'lifespan' => 100
                ]);
            } elseif ($bee->type == 'worker') {
                for ($i = 0; $i < 5; $i++) {
                    DB::table('bees')->insert([
                        'type_id' => $bee->id,
                        'lifespan' => 75
                    ]);
                }
            } elseif ($bee->type == 'drone') {
                for ($i = 0; $i < 8; $i++) {
                    DB::table('bees')->insert([
                        'type_id' => $bee->id,
                        'lifespan' => 50
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('bees')->where('id', '!=', 0)->delete();
    }
}
