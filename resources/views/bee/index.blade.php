<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bee Game</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>

    <div id="bee_wrapper">
       <div class="container">
           <div class="row">
               @if(Session::has('log'))
                   <div class="alert alert-danger">
                       {{ Session::get('log') }}
                   </div>
               @endif
           </div>
           @if($bees->count())
               <div class="row">
                   @foreach($bees as $bee)
                       <div class="col-md">
                           <div class="bee">
                               <p><strong>{{ $bee->id }}</strong></p>
                               <p>{{ $bee->type }}</p>
                               <p>{{ $bee->lifespan }} HP</p>
                           </div>
                       </div>
                   @endforeach
               </div>
               <div class="row">
                   <form action="{{ url('/hit-bee') }}" method="post">
                       @csrf
                       <button class="hit_btn btn btn-danger" type="submit">Hit!</button>
                   </form>
               </div>
           @else
               <div class="alert alert-warning">Please run migrations to insert bees.</div>
           @endif
       </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>